﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientUDP
{
    public partial class ClientForm : Form
    {
        private Controller ctrl;

        public ClientForm(Controller ctrl)
        {
            // TODO: Complete member initialization
            this.ctrl = ctrl;
            InitializeComponent();
            TextBox.CheckForIllegalCrossThreadCalls = false;
        }

        private void ClientForm_Load(object sender, EventArgs e)
        {
            btnSend.Enabled = false;

            cmbServers.Items.Add("127.0.0.1");
            cmbServers.Items.Add("localhost");
            cmbPort.Items.Add("4321");
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            ctrl.connect(cmbServers.Text, Convert.ToInt32(cmbPort.Text));

            ctrl.send(txtString1.Text+"|"+txtString2.Text);

            Thread crtThread = new Thread(receiveThread);

            crtThread.Start();
            
        }

        private void receiveThread()
        {
            txtReceive.Text = ctrl.receive();
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            try
            {
                ctrl.connect(cmbServers.Text, Convert.ToInt32(cmbPort.Text));
                MessageBox.Show("Server online");
                btnSend.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nu se poate trimite catre acest server momentan");
            }
        }

        private void cmbServers_MouseClick(object sender, MouseEventArgs e)
        {
            btnSend.Enabled = false;
        }

        private void cmbPort_MouseClick(object sender, MouseEventArgs e)
        {
            btnSend.Enabled = false;
        }

    }
}
