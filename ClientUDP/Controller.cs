﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClientUDP
{
    public class Controller
    {
        UdpClient udpClient;
        IPEndPoint remoteIpEndPoint;
        public Controller()
        {
            udpClient = new UdpClient();
            remoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
        }



        public void connect(String ip,int port)
        {
            //for example localhost
            udpClient.Connect(ip, port);
        }

        public void send(String text)
        {
            // Sends a message to the host to which you have connected.
            Byte[] sendBytes = Encoding.ASCII.GetBytes(text);

            udpClient.Send(sendBytes, sendBytes.Length);
        }
        public void receiveThread()
        {

        }
        public String receive()
        {
            // Blocks until a message returns on this socket from a remote host.
            Byte[] receiveBytes = udpClient.Receive(ref remoteIpEndPoint);
            String returnData = Encoding.ASCII.GetString(receiveBytes);

            return returnData.ToString();
        }

        public void close()
        {
            udpClient.Close();

        }
    }
}
