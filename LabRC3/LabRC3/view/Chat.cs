﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabRC3
{
    public partial class Chat : Form
    {
        Controller ctrl;
        int localport = 8080;
        public Chat(Controller ctrl)
        {
            InitializeComponent();
            this.ctrl = ctrl;
        }

        private void btnIPAdress_Click(object sender, EventArgs e)
        {
            lstNetInfo.Items.Clear();

            foreach(NetworkInfo netInfo in ctrl.getNetInfo()){
                lstNetInfo.Items.Add(netInfo);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NetworkInfo net = (NetworkInfo)lstNetInfo.SelectedItem;

            txtBroadCast.Text = net.BroadcastAddress.ToString();

            

            ChatForm ch = new ChatForm(net);


            ch.Show();
        }

        private void btnChat_Click(object sender, EventArgs e)
        {
        }

        private void Chat_FormClosing(object sender, FormClosingEventArgs e)
        {

        }



    }
}
