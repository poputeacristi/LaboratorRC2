﻿namespace LabRC3
{
    partial class Chat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnIPAdress = new System.Windows.Forms.Button();
            this.lstNetInfo = new System.Windows.Forms.ListBox();
            this.btnSelectNetwork = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBroadCast = new System.Windows.Forms.TextBox();
            this.txtNetwork = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnIPAdress
            // 
            this.btnIPAdress.Location = new System.Drawing.Point(12, 12);
            this.btnIPAdress.Name = "btnIPAdress";
            this.btnIPAdress.Size = new System.Drawing.Size(104, 23);
            this.btnIPAdress.TabIndex = 1;
            this.btnIPAdress.Text = "get NetworkInfo";
            this.btnIPAdress.UseVisualStyleBackColor = true;
            this.btnIPAdress.Click += new System.EventHandler(this.btnIPAdress_Click);
            // 
            // lstNetInfo
            // 
            this.lstNetInfo.FormattingEnabled = true;
            this.lstNetInfo.Location = new System.Drawing.Point(12, 50);
            this.lstNetInfo.Name = "lstNetInfo";
            this.lstNetInfo.Size = new System.Drawing.Size(239, 95);
            this.lstNetInfo.TabIndex = 2;
            // 
            // btnSelectNetwork
            // 
            this.btnSelectNetwork.Location = new System.Drawing.Point(88, 230);
            this.btnSelectNetwork.Name = "btnSelectNetwork";
            this.btnSelectNetwork.Size = new System.Drawing.Size(75, 23);
            this.btnSelectNetwork.TabIndex = 3;
            this.btnSelectNetwork.Text = "Chat";
            this.btnSelectNetwork.UseVisualStyleBackColor = true;
            this.btnSelectNetwork.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 182);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "BroadCast";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 207);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Network:";
            // 
            // txtBroadCast
            // 
            this.txtBroadCast.Location = new System.Drawing.Point(76, 179);
            this.txtBroadCast.Name = "txtBroadCast";
            this.txtBroadCast.ReadOnly = true;
            this.txtBroadCast.Size = new System.Drawing.Size(100, 20);
            this.txtBroadCast.TabIndex = 5;
            // 
            // txtNetwork
            // 
            this.txtNetwork.Location = new System.Drawing.Point(76, 204);
            this.txtNetwork.Name = "txtNetwork";
            this.txtNetwork.ReadOnly = true;
            this.txtNetwork.Size = new System.Drawing.Size(100, 20);
            this.txtNetwork.TabIndex = 5;
            // 
            // Chat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txtNetwork);
            this.Controls.Add(this.txtBroadCast);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSelectNetwork);
            this.Controls.Add(this.lstNetInfo);
            this.Controls.Add(this.btnIPAdress);
            this.Name = "Chat";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Chat_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIPAdress;
        private System.Windows.Forms.ListBox lstNetInfo;
        private System.Windows.Forms.Button btnSelectNetwork;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBroadCast;
        private System.Windows.Forms.TextBox txtNetwork;

    }
}

