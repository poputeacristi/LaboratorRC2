﻿using LabRC3.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabRC3
{
    public partial class ChatForm : Form
    {
        private volatile NetworkInfo net;
        private int listenPort = 1234;
        private List<User> users;
        
        Thread thread;

        public ChatForm(NetworkInfo net)
        {

            InitializeComponent();

            ListBox.CheckForIllegalCrossThreadCalls = false;
            this.net = net;
            lblBroadcast.Text = net.BroadcastAddress.ToString();

            thread = new Thread(StartListener);
            thread.IsBackground = true;
            thread.Start();

            users = new List<User>();
            users.Add(new User("Chris",net.HostAdress));

            time.Start();

            sendMessage("JOIN: Chris\0");

            loadUsers();
        }


        public void StartListener()
        {
            bool done = false;

            IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, 1234);
            UdpClient listener = new UdpClient();
            try
            {
                listener = new UdpClient(groupEP);
            }
            catch (Exception exp)
            {
                MessageBox.Show("Ai deja deschisa un fereastra de chat");
                this.Close();
 
            }
            try
            {
                while (!done)
                {
                    byte[] bytes = listener.Receive(ref groupEP);
                    
                    String text = Encoding.ASCII.GetString(bytes, 0, bytes.Length);
                    
                    int ok = 0;
                    String nume = "";
                    if (text[0] == 'J')
                    {
                        for (int i = 6; i < text.Length-1; i++)
                        {
                            nume += text[i];
                        }
                        User user = new User(nume, groupEP.Address);
                        foreach (User user1 in users)
                            if (user.Name == user1.Name)
                                ok = 1;
                        if (ok == 0)
                        {
                            lstUsers.Items.Add(user.Name);
                            users.Add(user);
                        }
                        loadUsers();
                    }
                    if (text[0] == 'M')
                    {
                        ok = 0;
                        foreach (User user in users) {
                            if (user.Adr.ToString() == groupEP.Address.ToString())
                            {
                                nume = user.Name;
                            }
                        }
                    }
                    if (text[0] == 'L')
                    {
                        foreach (User user in users)
                        {
                            if (user.Adr.ToString() == groupEP.Address.ToString())
                            {
                                users.Remove(user);
                                loadUsers();
                                break;
                            }
                        }
                    }

                    if (ok == 0) {
                        lstChat.Items.Add(groupEP.ToString() + "Name:" + nume + " " + text);}
                    ok = 0;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                listener.Close();
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            sendMessage("MESSAGE:"+txtChat.Text+"\0");

            lstChat.TopIndex = lstChat.Items.Count - 1;
        }

        private void sendMessage(string dataString)
        {
            IPEndPoint ipep = new IPEndPoint(net.BroadcastAddress, 1234);
            UdpClient client = new UdpClient();
            client.Connect(ipep);

            byte[] data = Encoding.ASCII.GetBytes(dataString);

            client.Send(data, data.Length);
        }

        private void ChatForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //thread.Interrupt();
            sendMessage("LEAVE\0");
        }

        private void loadUsers()
        {
            lstUsers.Items.Clear();
            if(users!=null)
            foreach (User user in users)
            {
                lstUsers.Items.Add(user);
            }
        }


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {

                sendMessage("MESSAGE:" + txtChat.Text);
                txtChat.Clear();
            }

            lstChat.TopIndex = lstChat.Items.Count-1;
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void ChatForm_Load(object sender, EventArgs e)
        {
            loadUsers();
        }

        private void time_Tick(object sender, EventArgs e)
        {

            sendMessage("JOIN: Chris\0");
        }

        private void userTimer_Tick(object sender, EventArgs e)
        {
            
        }
    }
}
