﻿using LabRC3.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LabRC3
{
    public class Controller
    {
        public Controller()
        {
            
        }


        #region Network info
        /*get the IPaddress and the subnetmask*/
        public List<NetworkInfo> getNetInfo()
        {

            List<NetworkInfo> networkList = new List<NetworkInfo>();
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    NetworkInfo netInfo = new NetworkInfo(ip, Util.getSubnetMask(ip));
                    networkList.Add(netInfo);
                }
            }
            return networkList;
        }

        #endregion

    }
}
