﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LabRC3.model
{
    public class User
    {
        private String name;

        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        private IPAddress adr;

        public IPAddress Adr
        {
            get { return adr; }
            set { adr = value; }
        }
        public User(String name, IPAddress adr)
        {
            this.name = name;
            this.adr = adr;
        }

        public override string ToString()
        {
            return "Name=" + name;
        }
    }
}
