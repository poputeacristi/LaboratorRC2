﻿using LabRC3.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LabRC3
{
    public class NetworkInfo
    {
        private IPAddress hostAdress;
        private IPAddress subnetmaskAdress;
        private IPAddress broadcastAddress;

        public NetworkInfo(IPAddress hostAdress, IPAddress subnetmaskAdress)
        {
            this.hostAdress = hostAdress;
            this.subnetmaskAdress = subnetmaskAdress;
            broadcastAddress = Util.getBroadcastAddress(hostAdress, subnetmaskAdress);
        }

        private string broadcastAdress()
        {

            return "her";
        }


        public IPAddress HostAdress
        {
            get { return hostAdress; }
            set { hostAdress = value; }
        }

        public IPAddress SubnetmaskAdress
        {
            get { return subnetmaskAdress; }
            set { subnetmaskAdress = value; }
        }

        public override String ToString()
        {
            return "HostIp: " + hostAdress + " NetmaskIp: " + subnetmaskAdress;
        }

        public IPAddress BroadcastAddress
        {
            get { return broadcastAddress; }
            set { broadcastAddress = value; }
        }
    }
}
