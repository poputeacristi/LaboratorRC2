﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UDP
{
    class Program
    {
        static void Main(string[] args)
        {
            UdpClient udpclient = new UdpClient(2015);
            Console.Write("Hey");
            udpclient.Connect("172.30.0.4", 2015);
            Console.Write("Hey");
            Byte[] sendBytes = Encoding.ASCII.GetBytes("pcir1549");

            Console.WriteLine(udpclient.Send(sendBytes, sendBytes.Length));

            //IPEndPoint object will allow us to read datagrams sent from any source.
            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

            // Blocks until a message returns on this socket from a remote host.
            string returnData="";
            try
            {
                Byte[] receiveBytes = udpclient.Receive(ref RemoteIpEndPoint);
                returnData = Encoding.ASCII.GetString(receiveBytes);
            }catch(Exception exp){
                Console.WriteLine(exp.Message);
            }
            // Uses the IPEndPoint object to determine which of these two hosts responded.
            Console.WriteLine("This is the message you received " +
                                         returnData.ToString());
            Console.Write("Hey");
            Console.Read();
        }
    }
}
