﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerUDP
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] data = new byte[1024];
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 4321);
            UdpClient newsock = new UdpClient(ipep);

            Console.WriteLine("Waiting for a client...");

            while (true)
            {
                data = newsock.Receive(ref ipep);

                ClientHandler client = new ClientHandler();

                String message = client.handle(Encoding.ASCII.GetString(data, 0, data.Length));

                //Console.WriteLine("IP ADDRESS:" + ipep.Address.ToString() + " Port number:" + ipep.Port.ToString());

                Console.WriteLine("Data received:" + Encoding.ASCII.GetString(data, 0, data.Length));

                data = Encoding.ASCII.GetBytes(message);
                newsock.Send(data, data.Length, ipep);
                
            }

        }
    }
}
