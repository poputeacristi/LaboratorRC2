﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerUDP
{
    public class ClientHandler
    {
        public ClientHandler()
        { 

        }

        public String handle(String text)
        {

            String[] strings = text.Split('|');

            return getCommonNUmbers(strings[0].Split(' '), strings[1].Split(' '));
        }

        public String getCommonNUmbers(String[] sir1,String[] sir2)
        {
            String rez="";
            foreach (String number1 in sir1)
                foreach (String number2 in sir2)
                    if (number1.Equals(number2))
                        rez += number1 + " ";
             
            return rez;
        }
    }
}
